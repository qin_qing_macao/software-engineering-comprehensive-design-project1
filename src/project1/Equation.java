package project1;
import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;
import java.util.function.BiFunction;

public class Equation {
	static final int UPPER=100;
	static final int LOWER=0;
	public Integer left_operand=0,right_operand=0,midlle_operand=0;
	public char operator1='+',operator2='+';
	private int value=0;
	public Equation() {
		// TODO Auto-generated constructor stub
		generateBinaryOperation(generateOperator(), generateOperator());
	}
	public char generateOperator(){
		Random random=new Random();
		int temp = random.nextInt(4);
		if(temp==0) 
			return '+';
		else if(temp==1)
			return '-';
		else if(temp==2)
			return '*';
		else
			return '/';
	}
	public void generateBinaryOperation(char op1, char op2) {//产生算式（加减法取决于参数）
		Random random=new Random();
		int num1,num2,num3,result;
		num1=random.nextInt(UPPER+1);
		num2=random.nextInt(UPPER+1);
		if(op1=='/'){
			while(num2==0)
				num2=random.nextInt(UPPER+1);
		}
		num3=random.nextInt(UPPER+1);
		if(op2=='/'){
			while(num3==0)
				num3=random.nextInt(UPPER+1);				
		}
		left_operand=num1;
		midlle_operand=num2;
		right_operand=num3;
		operator1=op1;
		operator2=op2;
		result=cal(num1, num2,num3,op1,op2);
		value=result;
	}
	public  char GetOperator() {//返回算式运算符
		return operator1;
	}
	public int getPriority(char operator) {
		if(operator=='/'||operator=='*')
			return 2;
		return 1;
	}
	public int cal(int num1,int num2,int num3,char op1,char op2) {
		int pri1=getPriority(op1),pri2=getPriority(op2);
		//System.out.println(op1+":"+pri1+" "+op2+":"+pri2);
		int res = num1;
		if(pri1>pri2||pri1==pri2){
			if(op1=='+')
				res=num1+num2;
			else if(op1=='-')
				res=num1-num2;
			else if(op1=='*')
				res=num1*num2;
			else
				res=num1/num2;			
			if(op2=='+')
				res+=num3;
			else if(op2=='-')
				res-=num3;
			else if(op2=='*')
				res*=num3;
			else
				res/=num3;	
		}else{
			if(op2=='+')
				res=num2+num3;
			else if(op2=='-')
				res=num2-num3;
			else if(op2=='*')
				res=num2*num3;
			else
				res=num2/num3;	
			if(op1=='+')
				res=num1+res;
			else if(op1=='-')
				res=num1-res;
			else if(op1=='*')
				res=num1*res;
			else
				res=num1/res;				
		}
		return res;
	}
	public int getResult(){
		return value;
	}
	public String toString() {
		String s;
		s=left_operand+""+operator1+midlle_operand+""+operator2+right_operand+"=";
		return s;
	}
}