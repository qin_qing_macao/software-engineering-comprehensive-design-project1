package project1;
import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;
import java.util.function.BiFunction;
import project1.Equation;
public class Main {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		int num,res;
		System.out.println("请输入要产生的算式的个数：");
		num = cin.nextInt();
		while(num-->0){
			Equation equation = new Equation();
			System.out.println(equation.toString());
			System.out.println("请输入你的答案：");
			res=cin.nextInt();
			if(res==equation.getResult())
				System.out.println("恭喜你答对了！");
			else
				System.out.println("很遗憾，你答错了！正确答案为："+equation.getResult());
		}

	}
}